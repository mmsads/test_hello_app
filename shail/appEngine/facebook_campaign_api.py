#Class for facebook campaign APIs

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote
from facebookads.api import FacebookAdsApi
from facebookads.objects import AdAccount
from facebookads.objects import AdUser
from facebookads.objects import AdCampaign

package = 'campaignPackage'

my_app_id = '1019596131397733'
my_app_secret = 'a16a85ae675d8abe40c797689489e6c3'
my_access_token = 'CAAOfUTk58GUBANGMtIjfNbrpWDMLaOOLwzkZAyOpHloFYe1dPZAhZAUZBESCwAUoqObm3dZCCVsaYEz2rOyl3rmWhczfRNejkqt3eT0rHXHNplLPecfQYVHEXffiZB5GWq9AYloJG2PqKpRhARq7wJtX7e6rfZCFYbmTycvUsLr724pphuOXQor8JLakyrY0UB4honZAc1on2RZAZAmvIAbRb3pbmDPFirglUZD'

fb_api = endpoints.api(name='fbApi', version='v1')

class CampaignInput(messages.Message):
    name = messages.StringField(1, required=True)
    status = messages.StringField(2)
    objective = messages.StringField(3)

class CampaignOutput(messages.Message):
    id = messages.StringField(1)
    name = messages.StringField(2)
    status = messages.StringField(3)
    objective = messages.StringField(4)

class CampaignOutputCollection(messages.Message):
    items = messages.MessageField(CampaignOutput, 1, repeated=True)

FB_AD_CAMPAIGN_FIELDS = [
        AdCampaign.Field.id,
        AdCampaign.Field.name,
        AdCampaign.Field.status,
        AdCampaign.Field.objective ]

@fb_api.api_class(resource_name='campaignApi')
class CampaignApi(remote.Service):

    ID_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage, id=messages.StringField(1))

    ####### LIST CAMPAIGN
    @endpoints.method(message_types.VoidMessage, CampaignOutputCollection,
            path='campaigns', http_method='GET', name='campaigns.list')
    def campaign_list(self, unused_request):
        my_ad_account = self.initialize()
        fb_campaigns = my_ad_account.get_ad_campaigns(fields=FB_AD_CAMPAIGN_FIELDS)
        fb_campaign_list = list(fb_campaigns)
        camp_collection = []
        for fb_campaign in fb_campaign_list:
            camp_collection.append(CampaignOutput(
                    id=fb_campaign[AdCampaign.Field.id],
                    name=fb_campaign[AdCampaign.Field.name],
                    status=fb_campaign[AdCampaign.Field.status],
                    objective=fb_campaign[AdCampaign.Field.objective]))

        return CampaignOutputCollection(items=camp_collection)

    ####### GET CAMPAIGN
    @endpoints.method(ID_RESOURCE, CampaignOutput, path='campaigns/{id}', http_method='GET',
            name='campaigns.get')
    def campaign_get(self, request):
        my_ad_account = self.initialize()

        fb_campaign = AdCampaign(request.id)
        fb_campaign.remote_read(fields=FB_AD_CAMPAIGN_FIELDS)
        return CampaignOutput(
                        id=fb_campaign[AdCampaign.Field.id],
                        name=fb_campaign[AdCampaign.Field.name],
                        status=fb_campaign[AdCampaign.Field.status],
                        objective=fb_campaign[AdCampaign.Field.objective])

    ####### CREATE CAMPAIGN
    @endpoints.method(endpoints.ResourceContainer(CampaignInput), CampaignOutput,
            path='campaigns/create', http_method='POST', name='campaigns.create')
    def campaign_create(self, request):
        my_ad_account = self.initialize()
        fb_campaign = AdCampaign(parent_id=my_ad_account[AdAccount.Field.id])
        fb_campaign[AdCampaign.Field.name] = request.name
        fb_campaign[AdCampaign.Field.status] = AdCampaign.Status.paused
        fb_campaign[AdCampaign.Field.objective] = AdCampaign.Objective.none
        fb_campaign.remote_create()

        return CampaignOutput(
                id=fb_campaign[AdCampaign.Field.id],
                name=fb_campaign[AdCampaign.Field.name],
                status=fb_campaign[AdCampaign.Field.status],
                objective=fb_campaign[AdCampaign.Field.objective])

    ####### DELETE CAMPAIGN
    @endpoints.method(ID_RESOURCE, message_types.VoidMessage, path='campaigns/{id}',
            http_method='DELETE', name='campaigns.delete')
    def campaign_delete(self, request):
        self.initialize()
        fb_campaign = AdCampaign(request.id)
        fb_campaign.remote_delete()
        return message_types.VoidMessage()

    # Initializes Facebook Apis
    def initialize(self):
        FacebookAdsApi.init(my_app_id, my_app_secret, my_access_token)
        me = AdUser(fbid='me')
        my_accounts = list(me.get_ad_accounts())
        return my_accounts[0];
